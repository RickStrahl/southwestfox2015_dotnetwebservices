﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using TestService;
using Westwind.Utilities;

namespace AlbumViewerServices
{
    /// <summary>
    /// Summary description for AlbumServiceCom
    /// </summary>
    [WebService(Namespace = "http://albumviewerservice/albumservicecom/asmx/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    public class AlbumServiceCom : System.Web.Services.WebService
    {

        [WebMethod]
        public Artist[] GetArtists(string artistName)
        {
            dynamic fox = CreateObject();

            dynamic artists = fox.GetArtists(artistName);
            var artistList = new List<Artist>();

            for (int i = 1; i <= artists.Count; i++)
            {
                dynamic foxArtist = artists.item(i);

                var artist = new Artist();

                artist.Id = (int)foxArtist.Id;
                artist.ArtistName = foxArtist.ArtistName;
                artist.Descriptio = foxArtist.Descriptio;

                artistList.Add(artist);
            }

            return artistList.ToArray();
        }

        [WebMethod]
        public Album[] GetAlbums(string albumName)
        {
            dynamic fox = CreateObject();
            dynamic albums = fox.GetAlbums(albumName);
            var albumList = new List<Album>();

            // IMPORTANT: Fox collections are 1 based!
            for (int i = 1; i <= albums.Count; i++)
            {
                dynamic foxAlbum = albums.item(i);

                var album = new Album();

                album.Id = (int)foxAlbum.Id;
                album.Title = foxAlbum.Title;
                album.Descriptio = foxAlbum.Descriptio;
                album.ArtistId = (int)foxAlbum.ArtistId;

                dynamic foxTracks = foxAlbum.Tracks;
                dynamic foxArtist = foxAlbum.Artist;

                album.Artist = new Artist();
                album.Artist.Id = (int)foxArtist.Id;
                album.Artist.ArtistName = foxArtist.ArtistName;

                album.Tracks = new List<Track>();

                // Fox collection is 1 based
                for (int j = 1; j <= foxTracks.Count; j++)
                {
                    dynamic foxTrack = foxTracks.Item(j);
                    var track = new Track();
                    track.Id = (int)foxTrack.Id;
                    track.SongName = foxTrack.SongName;
                    track.Length = foxTrack.Length;

                    album.Tracks.Add(track);
                }

                albumList.Add(album);
            }

            return albumList.ToArray();
        }

        [WebMethod]
        public Artist GetArtist(int artistId)
        {
            dynamic fox = CreateObject();
            dynamic foxArtist = fox.GetArtist(artistId);

            // COM nulls come back as DbNull objects
            if (foxArtist is DBNull)
                return null;

            var artist = new Artist();

            artist.Id = (int)foxArtist.Id;
            artist.ArtistName = foxArtist.ArtistName;
            artist.Descriptio = foxArtist.Descriptio;
            artist.AmazonUrl = foxArtist.AmazonUrl;
            artist.ImageUrl = foxArtist.ImageUrl;

            return artist;
        }

        [WebMethod]
        public int SaveArtist(Artist artist)
        {
            dynamic fox = CreateObject();

            int result;
            result = (int)fox.SaveArtist(artist);

            return result;
        }

        [WebMethod]
        public int GenerateError()
        {
            // generate error with standard .NET exception
            throw new ArgumentException("Custom error generated from .NET");

            return 0;
        }

        private dynamic CreateObject(string progid = "Albums.AlbumServer")
        {
            Type type = Type.GetTypeFromProgID(progid, true);
            return Activator.CreateInstance(type);
        }

    }
}
