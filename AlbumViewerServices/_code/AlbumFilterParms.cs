namespace AlbumViewerServices
{
    public class AlbumFilterParms
    {
        public string AlbumName { get; set; }

        public int AlbumYear { get; set; }
        public string ArtistName { get; set; }

        public string TrackName { get; set; }
    }
}