﻿using System;
using Westwind.Utilities.Configuration;

namespace AlbumViewerServices
{

    public class App
    {
        public static Configuration Configuration { get; set; }

        static App()
        {
            Configuration = new Configuration();
            Configuration.Initialize();
        } 
    }

    public class Configuration : AppConfiguration
    {
        public ProcessingModes ProcessingMode { get; set; }

        public Configuration()
        {
            ProcessingMode = ProcessingModes.FoxProOleDb;
        }

    }

    public enum ProcessingModes
    {
        FoxProOleDb,
        ComInterop,
        Dotnet
    }

}