﻿
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace TestService
{

public class Artist
{
    public int Id { get; set; }

    public string ArtistName { get; set; }
    public string Descriptio { get; set; }        
    public string ImageUrl { get; set; }        
    public string AmazonUrl { get; set; }
}

public class Album
{
    public int Id { get; set; }

    public string Title { get; set; }
    public string Descriptio { get; set; }
    public int Year { get; set; }
    public string ImageUrl { get; set; }
    public string AmazonUrl { get; set; }
    public string SpotifyUrl { get; set; }

    public int? ArtistId { get; set; }

    public virtual Artist Artist { get; set; }
    public virtual List<Track> Tracks { get; set; }        
}

public class Track
{
    public int Id { get; set; }

    public int? AlbumId { get; set; }
    public string SongName { get; set; }        
    public string Length { get; set; }
    public int Bytes { get; set; }
    public decimal UnitPrice { get; set; }		
}

}