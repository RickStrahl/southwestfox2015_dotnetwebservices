CLEAR
do wwDotNetBridge
LOCAL loBridge as wwDotNetBridge
loBridge = CreateObject("wwDotNetBridge","V4")


IF !loBridge.LoadAssembly("AlbumServiceProxy.dll")
    ? "Invalid library..." + loBridge.cErrorMsg
    return
ENDIF   

LOCAL loService as AlbumServiceProxy.Proxy.AlbumService
loService = loBridge.CreateInstance("AlbumServiceProxy.Proxy.AlbumService")


IF ISNULL(loService)
   ? "Unable to create service: " + loService.cErrorMsg
   RETURN
ENDIF
   
LOCAL loComplex as AlbumServiceProxy.Proxy.Complex
loComplex = loService.GetComplexType()

? "Long: " 
*? loComplex.LongNumber
? loBridge.GetProperty(loComplex,"LongNumber")

? "Single: " 
? loComplex.Single

? "Float: "
? loComplex.Float

? "Char: " 
? loComplex.SingleChar  && number
? loBridge.GetProperty(loComplex,"SingleChar")   && char


? "Color: "
? loComplex.SailStruct.Color
? loBridge.GetProperty(loComplex,"SailStruct.Color")   && char

? "Struct Decimal: " 
? loComplex.SailStruct.Size

? "Nullable Int: "
? loComplex.NullableInt

? "Guid: "
* ? loComplex.Guid
* Get a ComGuid
loGuid = loBridge.GetProperty(loComplex,"Guid")
? loGuid.GuidString

? "Strings: "
loStrings = loBridge.GetProperty(loComplex,"Strings")
? loStrings.Count
FOR lnX = 0 TO loStrings.Count -1
   ? loStrings.Item(lnX)
ENDFOR

? "DONE"