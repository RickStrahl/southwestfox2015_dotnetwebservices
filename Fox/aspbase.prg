SET PROCEDURE TO ASPBASE ADDITIVE

*** Set this flag to enable Error method on this class so errors
*** are handled
#DEFINE HANDLE_ERRORS .F.

*** Constants
#DEFINE CRLF CHR(13) + CHR(10)
*** End Constants


*************************************************************
DEFINE CLASS AspBase AS Session
*************************************************************
***    Author: Rick Strahl
***            (c) West Wind Technologies, 1999
***   Contact: (541) 386-2087  / rstrahl@west-wind.com
***  Modified: 12/08/98
***  Function: ASP Base class that provides basic
***            requirements for safe execution of a COM
***            component in ASP
*************************************************************

*** Hide all VFP members except Class
PROTECTED AddObject, AddProperty, Destroy, Error, NewObject, ReadExpression, Readmethod, RemoveObject, ResetToDefault, SaveAsClass, WriteExpression, WriteMethod, BaseClass, ClassLibrary, Comment, Controls, ControlCount, Height, Width, HelpContextId, Objects, Parent, ParentClass, Picture, ShowWhatsThis, WhatsThisHelpId

*** Base Application Path
ApplicationPath = "" 

*** .T. if an error occurs during call
IsError = .F.

*** Error message if an error occurs
ErrorMessage = "" 

*** Name of the Visual FoxPro Class - same as Class but Class is hidden
cClass = "" 

ComHelper = null


************************************************************************
* WebTools :: Init
*********************************
***  Function: Set the server's environment. IMPORTANT!
************************************************************************
FUNCTION Init

*** Expose Class publically 
this.cClass = this.Class

*** Make all required environment settings here
*** KEEP IT SIMPLE: Remember your object is created
***                 on EVERY ASP page hit!
IF Application.StartMode > 1  && Interactive or VFP IDE COM
	SET RESOURCE OFF  && best to do in compiled-in CONFIG.FPW
ENDIF

*** SET YOUR ENVIRONMENT HERE (for all)
*** or override in your subclassed INIT() 
SET EXCLUSIVE OFF
SET DELETED ON
SET EXACT OFF
SET SAFETY OFF

*** Retrieve the DLL location and grab just the path to save!
THIS.ApplicationPath = GetApplicationPath() && ADDBS( JustPath(Application.ServerName) )

*** Com Helper that helps with Conversions of cursors
THIS.ComHelper = CREATEOBJECT("ComHelper")

*** Add the startup path to the path list!
*** Add any additional relative paths as required
SET PATH TO ( THIS.ApplicationPath ) ADDITIVE

ENDFUNC
* Init

#IF HANDLE_ERRORS

************************************************************************
* aspBase :: Error
*********************************
***  Function: Error Method. Capture errors here in a string that
***            you can read from the ASP page to check for errors.
************************************************************************
FUNCTION ERROR
LPARAMETER nError, cMethod, nLine

this.ErrorMessage = MESSAGE()
this.IsError = .t.
RETURN

this.SetError(message() + Message(1) + " " + CHR(10) + CHR(13) +;
			  "Error: " + STR(nError) + "  Method: " + cMethod + "   Line: " +STR(nLine))

COMRETURNERROR("source unknown",this.ErrorMessage)
ENDFUNC

#ENDIF

************************************************************************
*  Log
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION Log(lcString, lcFileName)

IF EMPTY(lcString)
   lcString = "***"
ENDIF   
IF EMPTY(lcFileName)
   lcFileName = this.ApplicationPath + "Tracelog.txt"
ENDIF
   
STRTOFILE(lcString + CHR(13) + CHR(10),lcFileName,1)
ENDFUN
*   Log

************************************************************************
*  SetError
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
PROTECTED FUNCTION SetError(lcMessage, lcMethod, lnLine)

IF ISNULL(lcMessage) OR EMPTY(lcMessage)
   this.ErrorMessage = ""
   this.IsError = .F.
   RETURN
ENDIF   

THIS.IsError = .T.
THIS.ErrorMessage= lcMessage

ENDFUNC
*   SetError

ENDDEFINE
* AspBase


*************************************************************
DEFINE CLASS ComHelper AS Custom
*************************************************************
*: Author: Rick Strahl
*:         (c) West Wind Technologies, 2012
*:Contact: http://www.west-wind.com
*:Created: 09/14/2012
*************************************************************

*** ASP classic scripting context
PROTECTED ScriptingContext
ScriptingContext = .NULL.

************************************************************************
*  CreateObject
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION CreateObject(lcProgId as string)
LOCAL loObj
loObj = CREATEOBJECT(lcProgId)
IF VARTYPE(loObj) # "O"
   RETURN null
ENDIF
RETURN loObj
ENDFUNC
*   CreateObject

************************************************************************
*  Eval
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION Eval(lcEvalString)
RETURN EVALUATE(lcEvalString)
ENDFUNC
*   Eval

************************************************************************
*  CursorToCollection
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION CursorToCollection(lcAlias as String) as Object
RETURN CursorToCollection(lcAlias)
ENDFUNC
*   CursorToCollection

************************************************************************
*  CursorToXmlString
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION CursorToXmlString(lcAlias as string) as string
RETURN CursorToXmlString(lcAlias)
ENDFUNC
*   CursorToXmlString

************************************************************************
*  XmlStringToCursor
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION XmlStringToCursor(lcXmlString as String, lcTableName as String) as Boolean
RETURN XmlStringToCursor(lcXmlString, lcTableName)
ENDFUNC
*   XmlStringToCursor

************************************************************************
* AspBase ::  GetScriptingContext
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION GetScriptingContext()

IF !ISNULL(this.ScriptingContext)
   RETURN this.ScriptingContext
ENDIF   

*** If you want the ASP scripting context
*** you have to create it here
oMTS  = CreateObject("MTxAS.AppServer.1")
this.ScriptingContext = oMTS.GetObjectContext()

RETURN this.ScriptingContext
ENDFUNC
*  AspBase ::  GetScriptcontext


ENDDEFINE
*EOC ComHelper 



************************************************************************
FUNCTION GetApplicationPath
*********************************
***  Function: Returns the FoxPro start path
***            of the *APPLICATION*
***            under all startmodes supported by VFP.
***            Returns the path of the starting EXE, 
***            DLL, APP, PRG/FXP
***    Return: Path as a string with trailing "\"
************************************************************************

DO CASE 
   *** VFP 6 and later provides ServerName property for COM servers EXE/DLL/MTDLL
   CASE INLIST(Application.StartMode,2,3,5) 
         lcPath = JustPath(Application.ServerName)

   *** Interactive
   CASE (Application.StartMode) = 0
         lcPath = SYS(5) + CURDIR()
         
   *** Active Document
   CASE ATC(".APP",SYS(16,0)) > 0
       lcPath = JustPath(SYS(16,0))

  *** Standalone EXE or VFP Development
  OTHERWISE
       lcPath = JustPath(SYS(16,0))
       IF ATC("PROCEDURE",lcPath) > 0
         lcPath = SUBSTR(lcPath,RAT(":",lcPath)-1)
       ENDIF
ENDCASE

RETURN ADDBS(lcPath)
* EOF GetAppStartPath      


************************************************************************
* FoxSampleServer ::  ShowCursor
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION ShowCursor(lcWidth as String) as string
LOCAL lcOutput, lnFields, lcFieldname,x

IF EMPTY(lcWidth)
   lcWidth = "90%"
ENDIF
   
lcOutput = ;
  [<center>] + CRLF +;
  [<table class="blackborder" Width="] + lcWidth + [" cellpadding="4" rules="all" style="text-align:left">] +  CRLF


IF EMPTY(ALIAS())
   RETURN ""
ENDIF
   
lnFields = AFIELDS(laFields)

*** Build the header first
lcOutput = lcOutput + [<tr class="gridheader">]
FOR x=1 to lnFields
   lcfieldname=Proper(STRTRAN(lafields[x,1],"_"," "))
   lcOutput = lcOutput + "<th>"+lcFieldName+"</th>"
ENDFOR
lcOutput = lcOutput + "</td></tr>"

llAlternate = .F.
SCAN
    lcOutput = lcOutput + [<tr valign="Top" ] + IIF(llAlternate,[class="gridalternate" ],[ ]) + [>] + CRLF
    llAlternate = !llAlternate
    
	*** Just loop through fields and display
	FOR x=1 to lnFields
	         lcfieldtype=lafields[x,2]
             lcfieldname=lafields[x,1]
	         lvValue=EVAL(lcfieldname)
	         
	         DO CASE
	         CASE lcFieldType = "C"
	             IF EMPTY(lvValue)
	                lvValue = "&nbsp;"
	             ENDIF
	             lcOutput = lcOutput + "<td>" + lvValue + "</td>"
	         CASE lcFieldType = "M"
	             IF EMPTY(lvValue)
	                lvValue = "&nbsp;"
	             ENDIF
	             lcOutput = lcOutput + "<td>" + STRTRAN(lvValue,CHR(13),"<br>") + "</td>"
	         CASE lcFieldType = "G"
	             LOOP    
	         OTHERWISE   
    	         lcOutput = lcOutput + "<td>" + TRANSFORM(lvValue) + "</td>"
    	     ENDCASE
	ENDFOR && x=1 to lnFieldCount

    lcOutput = lcOutput + "</tr>" + CRLF
ENDSCAN

RETURN lcOutput + "</table>" + CRLF
ENDFUNC
*  FoxSampleServer ::  ShowCursor



************************************************************************
* CursorToXmlString
****************************************
***  Function: A generic routine that returns an XmlAdapter XML result
***            of a single table that can be loaded into a .NET 
***            DataSet. The table is encoded with the name of the
***            alias.
***      Pass: lcAlias  - Optional: Name of the cursor alias to encode
***    Return: Xml string
************************************************************************
FUNCTION CursorToXmlString(lcAlias)
LOCAL lcXML

IF EMPTY(lcAlias)
  lcAlias = ALIAS()
ENDIF

lcXML = ""
LOCAL oXA as XMLAdapter
oXA = CREATEOBJECT("XMLAdapter")
*** lcTable doesn't exist - error here (use ALIAS() )
oXA.AddTableSchema(lcAlias,.T.,STRCONV(lcAlias,5))
oXA.ToXML("lcXML")
RETURN lcXML
ENDFUNC
*  FoxSampleServer ::  CursorToXml

************************************************************************
*  CursorToCollection
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION CursorToCollection(lcCursor)
LOCAL loItem, loCol as Collection

loCol = CREATEOBJECT("Collection")
IF EMPTY(lcCursor)
  lcCursor = ALIAS()
ENDIF
  
SELECT (lcCursor)
SCAN
   	SCATTER NAME loItem MEMO
   	loCol.Add(loItem)
ENDSCAN

RETURN loCol
ENDFUNC
*   CursorToCollection

************************************************************************
*XmlStringToCursor
****************************************
***  Function:
***    Assume:
***      Pass:
***    Return:
************************************************************************
FUNCTION XmlStringToCursor(lcXML as String, lcTableName as String)
LOCAL lIsError

lIsError = .F.
TRY
	LOCAL oXA as XMLAdapter 
	oXA = CREATEOBJECT("XMLAdapter")

	oXA.LoadXml(lcXML)  
	oXA.Tables.Item(1).ToCursor(.F.,lcTableName)
CATCH
	lIsError = .t.
ENDTRY

RETURN !lIsError
ENDFUNC
*  AspBase ::  XmlStringToCursor



************************************************************************
FUNCTION Path
******************
***  Function: Adds or deletes items from the path string
***      Pass: pcPathName   -   Filename
***            pcMethod     -   *"ADD","DELETE"
***    Return: New Path or ""
************************************************************************
PARAMETERS pcPath,pcMethod
LOCAL lcOldPath

IF VARTYPE(pcMethod) # "C"
   pcMethod = "ADD"
ENDIF

IF EMPTY(pcPath)
   RETURN
ENDIF

pcPath=ADDBS(LOWER(TRIM(pcPath)))

IF pcMethod = "ADD"
   IF !EMPTY(pcPath)	
	   SET PATH TO (pcPath) ADDITIVE
   	   RETURN SET("PATH")
   ENDIF
ENDIF

lcOldPath=LOWER(SET("PATH"))
IF AT(";" + pcPath + ";" ,";" + lcOldPath + ";") < 1
	RETURN ""
ENDIF
lcOldPath=STRTRAN(lcOldPath + ";" ,";" +pcPath+";",";")
lcOldPath = SUBSTR(lcOldPath,1,LEN(lcOldPath)-1)

SET PATH TO &lcOldPath
RETURN lcOldPath
*EOP PATH
