CLEAR
do wwDotNetBridge
LOCAL loBridge as wwDotNetBridge
loBridge = CreateObject("wwDotNetBridge","V4")


IF !loBridge.LoadAssembly("AlbumServiceProxy.dll")
    ? "Invalid library..." + loBridge.cErrorMsg
    return
ENDIF   

LOCAL loService as AlbumServiceProxy.Proxy.AlbumService
loService = loBridge.CreateInstance("AlbumServiceProxy.Proxy.AlbumService")
loService.


*** Always check for errors!
IF ISNULL(loService)
   ? "Unable to create service: " + loService.cErrorMsg
   RETURN
ENDIF
   
loAlbum =  loService.GetAlbum("Ace of Spades")

*** Always check for errors
IF ISNULL(loAlbum)
  ? "Couldn't get item: " + loService.cErrorMsg
  RETURN
ENDIF

? loAlbum.Title
? "by " + loAlbum.Artist.ArtistName
? loAlbum.Descriptio
? loAlbum.Year


*** Get tracks Array as a COM Collection
loTracks = loBridge.GetProperty(loAlbum,"Tracks")

IF loTracks.Count > 0
	FOR lnX = 0 TO loTracks.Count - 1
		loTrack = loTracks.Item(lnX)
		? "   " + loTrack.SongName + " " + loTrack.Length
	ENDFO
ENDIF
  

RETURN