LPARAMETERS lcUrl

CLEAR
do wwDotNetBridge
LOCAL loBridge as wwDotNetBridge
loBridge = CreateObject("wwDotNetBridge","V4")

IF !loBridge.LoadAssembly("AlbumServiceProxy.dll")
    ? "Invalid library..." + loBridge.cErrorMsg
    return
ENDIF   

LOCAL loServer as AlbumServiceProxy.Proxy.AlbumService
loService = loBridge.CreateInstance("AlbumServiceProxy.Proxy.AlbumService")


*** Always check for errors!
IF ISNULL(loService)
   ? "Unable to create service: " + loBridge.cErrorMsg
   RETURN NULL
ENDIF

IF !EMPTY(lcUrl)
   loBridge.SetProperty(loService,"Url",lcUrl) 
ENDIF


*loAlbums = loService.GetAlbums()

loAlbums = loBridge.InvokeMethod(loService,"GetAlbums")
   
*** Always check for errors
IF ISNULL(loAlbums)
  ? "Couldn't get item: " + loService.cErrorMsg
  RETURN
ENDIF

FOR lnX = 0 TO loAlbums.Count - 1

    loAlbum = loAlbums.Item(lnx)
    
	? loAlbum.Title
	? "by " + loAlbum.Artist.ArtistName
	? loAlbum.Descriptio
	? loAlbum.Year

	*** Get tracks Array as a COM Collection
	loTracks = loBridge.GetProperty(loAlbum,"Tracks")

	IF loTracks.Count > 0
		FOR lnY = 0 TO loTracks.Count - 1
			loTrack = loTracks.Item(lnY)
			? "   " + loTrack.SongName + " " + loTrack.Length
		ENDFO
	ENDIF
ENDFOR
	  
RETURN