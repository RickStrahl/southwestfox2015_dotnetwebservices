CLEAR
do wwDotNetBridge
LOCAL loBridge as wwDotNetBridge
loBridge = CreateObject("wwDotNetBridge","V4")


IF !loBridge.LoadAssembly("AlbumServiceProxy.dll")
    ? "Invalid library..." + loBridge.cErrorMsg
    return
ENDIF   

LOCAL loService as AlbumServiceProxy.Proxy.AlbumService
loService = loBridge.CreateInstance("AlbumServiceProxy.Proxy.AlbumService")

IF ISNULL(loService)
   ? "Unable to create service: " + loService.cErrorMsg
   RETURN
ENDIF
   
loArtists =  loBridge.InvokeMethod(loService,"GetArtists","")
? loArtists.Count

FOR lnX = 0 to loArtists.Count - 1
	loArtist = loArtists.Item(lnx)
	? "   " + loArtist.ArtistNAme
ENDFOR 

RETURN