CLEAR
do wwDotNetBridge
LOCAL loBridge as wwDotNetBridge
loBridge = CreateObject("wwDotNetBridge","V4")


IF !loBridge.LoadAssembly("AlbumServiceProxy.dll")
    ? "Invalid library..." + loBridge.cErrorMsg
    return
ENDIF   

LOCAL loService as AlbumServiceProxy.Proxy.AlbumService
loService = loBridge.CreateInstance("AlbumServiceProxy.Proxy.AlbumService")

IF ISNULL(loService)
   ? "Unable to create service: " + loService.cErrorMsg
   RETURN
ENDIF
   
LOCAL loComplex as AlbumServiceProxy.Proxy.Complex
loComplex = loBridge.CreateInstance("AlbumServiceProxy.Proxy.Complex")

LOCAL loComValue as Westwind.WebConnection.ComValue
loComValue = loBridge.CreateInstance("Westwind.WebConnection.ComValue")

loComplex.Byte = 2
loComplex.Float = 10
loComplex.Unsigned64BitInteger = 20

loComplex.SingleChar = 49  && Must pass char as number
loBridge.SetProperty(loComplex,"SingleChar",loComValue.SetChar("B"))

loComplex.Single = 10.11
loComplex.LongNumber = 10

*** Nullable set to null requires lobridge
*loComplex.NullableInt = null  && doesn't work
loBridge.SetProperty(loComplex,"NullableInt",Null)
loComplex.NullableInt = 10  && just works


*loComplex.Bytes = CREATEBINARY("abcs")
loBridge.SetProperty(loComplex,"Bytes",CREATEBINARY("abcs"))

*** Assign Enum by number or by string type name
loComplex.Color= 1  && Colors_Green
loComplex.Color = loBridge.GetEnumValue("AlbumServiceProxy.Proxy.Colors.Orange")  


loComValue.NewGuid()

* loComGuid = loBridge.CreateInstance("Westwind.WebConnection.ComGuid")
* loComGuid.New()
loBridge.SetProperty(loComplex,"Guid",loComValue)


*** String array - create a COM array
loStrings = loBridge.CreateArray("System.String")
loStrings.AddItem("FoxPro")
loStrings.AddItem("Web Services")
loStrings.AddItem(".NET")

loBridge.SetProperty(loComplex,"Strings",loStrings)

LOCAL loSail as AlbumServiceProxy.Proxy.SailStruct
loSail = loBridge.CreateInstance("AlbumServiceProxy.Proxy.SailStruct")

loSail.Color= 3  && Colors_Black
loSail.MastSize =  400
loSail.Size = 3.2

loComplex.SailStruct =  loSail

? loService.PassComplexType(loComplex)

RETURN

? "Long: " 
*? loComplex.LongNumber
? loBridge.GetProperty(loComplex,"LongNumber")

? "Single: " 
? loComplex.Single

? "Float: "
? loComplex.Float

? "Char: " 
? loComplex.SingleChar  && number
? loBridge.GetProperty(loComplex,"SingleChar")   && char


? "Color: "
? loComplex.SailStruct.Color
? loBridge.GetProperty(loComplex,"SailStruct.Color")   && char

? "Struct Decimal: " 
? loComplex.SailStruct.Size

? "Nullable Int: "
? loComplex.NullableInt

RETURN