
lcPath = "..\albumviewerservices\app_data\"

SET CLASSLIB TO WWSQL ADDITIVE

LOCAL loSql as wwsql
oSql = CREATEOBJECT("wwSQL")

? oSql.Connect("database=AlbumViewer")
? oSql.EnableUnicodeToAnsiMapping() 

? oSql.Execute("select * from albums")
COPY TO (lcPath + "albums.dbf")


? oSql.Execute("select * from artists")
COPY TO (lcPath + "artists.dbf")

? oSql.Execute("select * from tracks")
COPY TO (lcPath + "tracks")


RETURN