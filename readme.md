# Creating and Consuming Web Services with Visual FoxPro and .NET

### Session notes, Slides and samples from the Southwest Fox 2015 session.


* [Session Notes](https://bitbucket.org/RickStrahl/southwestfox2015_dotnetwebservices/raw/875e73edf5233d5f5df2ef2e6d3200d26f3753e8/Documents/CreatingAndConsumingFoxProWebServicesWithDotNet.pdf)
* [Session Slides](https://bitbucket.org/RickStrahl/southwestfox2015_dotnetwebservices/raw/875e73edf5233d5f5df2ef2e6d3200d26f3753e8/Documents/Strahl_FoxProDotnetWebServices.pptx)

Related content:

* [ASP.NET Interop Session Notes](https://bitbucket.org/RickStrahl/southwestfox2015_dotnetwebservices/raw/875e73edf5233d5f5df2ef2e6d3200d26f3753e8/Documents/AspNetInterop/AspNetFoxProRevisited.pdf)